##########################
# Global aliases    /etc/profile.d/globalaliases.sh
##########################
alias addr="ip addr"
alias apagar="shutdown -h now"
alias cd.="cd ."
alias cd..="cd .."
alias df="df -h"
alias du="du -h"
alias h="history"
alias ls="ls -lahF --color"
alias l="ls"
alias network-scripts="cd /etc/sysconfig/network-scripts"
alias q="exit"
alias reiniciar="shutdown -r now"
alias uname="uname -a"
alias update="yum -y update"