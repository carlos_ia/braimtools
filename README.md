# SCRIPTS #

Scripts have been revised and functions have been added. They have been checked on the supported OS:

* epelinstall | Description:  Script to install EPEL repository | OS Supported: CentOS and RHEL 6, 7 and 8
* initsetup | Description: initial setup of basic tools to manage CentOS or RHEL, configuration of aliases, pre-login message, and ssh root restriction | OS Supported: CentOS and RHEL 6, 7 or 8
* lampinstall | Description:  Script for installing Apache, MySQL, PHP (LAMP Stack). PHP version is 7.4. To change it update function "installPHP" | OS Supported: CentOS and RHEL 7 and 8
* webmininstall | Description: Script to install webmin | OS Supported: CentOS and RHEL 6, 7 or 8

The rest of the scripts were used for some project and they have not been updated and documented yet.

--------------------

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact