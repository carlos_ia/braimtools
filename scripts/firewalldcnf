#!/bin/sh
# 
# Desciption: Script to setup firewall rules
# OS Supported: Any with firewalld
# Author: Carlos Ibrahim <carlos@braimtec.com>
#
# eth_ext - es la puerta de enlace a Internet
# eth_int - es la puerta local

# Check if user is root
if [ $(id -u) -ne 0 ] ; then
    echo "User must be root to run this script"
    exit 1
fi

# Set variables
osDistro=$(cat /etc/*-release | grep ID_LIKE | awk -F\" '{ print $2 }' | awk '{ print $1 }')
osVersion=$(cat /etc/*-release | grep VERSION_ID | awk -F\" '{ print $2 }')

eth_ext=ens192
eth_int=ens224
subnet_int=10.0.1.0/24

# Check distro
if [ ! $osDistro == "rhel" ] ; then
    echo "Linux distribution is not RHEL, CentOS or Fedora"
    exit 1
fi

# TCP 20, 21  - FTP (Datos, Control)
#firewall-cmd --add-port=20/tcp --permanent
#firewall-cmd --add-port=21/tcp --permanent

# TCP 22 - SSH
#firewall-cmd --add-port=22/tcp --permanent

# TCP, UDP 53 - DNS
firewall-cmd --add-port=53/tcp --permanent
firewall-cmd --add-port=53/udp --permanent

# UDP 68 - DHCP (Solicitudes clientes)
firewall-cmd --add-port=68/udp --permanent

# TCP 80  - HTTP
#firewall-cmd --add-port=80/tcp --permanent

# TCP, UDP 88 - SMB - Kerberos
#firewall-cmd --add-port=88/tcp --permanent
#firewall-cmd --add-port=88/udp --permanent

# UDP 123 - NTP
#firewall-cmd --add-port=123/udp --permanent

# TCP 135 - SMB - End Point Mapper (DCE/RPC Locator Service)
#firewall-cmd --add-port=135/tcp --permanent

# UDP 137 - SMB - NetBIOS name service
#firewall-cmd --add-port=137/udp --permanent

# UDP 138 - SMB - NetBIOS Datagram
#firewall-cmd --add-port=138/udp --permanent

# TCP 139 - SMB - NetBIOS Datagram
#firewall-cmd --add-port=139/tcp --permanent

# TCP, UDP 389 - SMB - LDAP
#firewall-cmd --add-port=389/tcp --permanent
#firewall-cmd --add-port=389/udp --permanent

# TCP 443 - HTTPS
#firewall-cmd --add-port=443/tcp --permanent

# TCP 445 - SMB - SMB over TCP
#firewall-cmd --add-port=445/tcp --permanent

# TCP, UDP 464 - SMB - Kerberos kpasswd
#firewall-cmd --add-port=464/tcp --permanent
#firewall-cmd --add-port=464/udp --permanent

# TCP, UDP 631 - CUPS server
#firewall-cmd --add-port=631/tcp --permanent
#firewall-cmd --add-port=631/udp --permanent

# TCP 636 - SMB - LDAPS (only if "tls enabled = yes")
#firewall-cmd --add-port=636/tcp --permanent

# TCP 1024 - SMB - Dynamic RPC Ports (1024-5000, abrir solo el 1024)
#firewall-cmd --add-port=1024/tcp --permanent

# TCP 3268 - SMB - Global Cataloge
#firewall-cmd --add-port=3268/tcp --permanent

# TCP 3269 - SMB - Global Cataloge SSL (only if "tls enabled = yes")
#firewall-cmd --add-port=3269/tcp --permanent

# TCP, UDP 5353 - SMB - Multicast DNS
#firewall-cmd --add-port=5353/tcp --permanent
#firewall-cmd --add-port=5353/udp --permanent

# TCP 10000 - Webmin
#firewall-cmd --add-port=10000/tcp --permanent

#################################################################################################
# Reglas FORWARD (Tabla: filter) y POSTROUTING (Tabla: nat) - Renvio de paquetes entre subredes #
#################################################################################################

# Enable IPv4 packet forwarding
#sysctl -w net.ipv4.ip_forward=1
#echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.d/ip_forward.conf

# Add direct rules to firewalld. Add the --permanent option to keep these rules across restarts
firewall-cmd --permanent --direct --add-rule ipv4 nat POSTROUTING 0 -o $eth_ext -j MASQUERADE
firewall-cmd --permanent --direct --add-rule ipv4 filter FORWARD 0 -i $eth_int -o $eth_ext -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter FORWARD 0 -i $eth_ext -o $eth_int -m state --state RELATED,ESTABLISHED -j ACCEPT

# Mensaje para recordar archivos a modificar
#echo "Recuerde habilitar el forward de paquetes en nuestro firewall cambiando el parametro net.ipv4.ip_forward = 1"

# Reiniciamos el firewall
firewall-cmd --reload
