#!/bin/bash
#
# Description: Install kimchi and dependencies
# OS Supported: CentOS 6, CentOS 7
# Autor: Carlos Ibrahim Arias <carlos@braimtec.com>

# Check if user is root
if [ $(id -u) -ne 0 ] ; then
    echo "User must be root to run this script"
    exit 1 
fi

# Set variables
osDistro=$(cat /etc/*-release | grep ID_LIKE | awk -F\" '{ print $2 }' | awk '{ print $1 }')
osVersion=$(cat /etc/*-release | grep VERSION_ID | awk -F\" '{ print $2 }')
epelRepoURL="http://dl.fedoraproject.org/pub/epel/$osVersion/x86_64"
epelRepo="epel.repo"
epelRPM6="epel-release-6-8.noarch.rpm"
epelRPM7="epel-release-7-5.noarch.rpm"
kimchiURL="http://kimchi-project.github.io/kimchi/downloads"
kimchiRPM="kimchi-1.4.0-0.el7.x86_64.rpm"

# Check distro
if [ ! $osDistro == "rhel" ] ; then
    echo "Linux distribution is not RHEL, CentOS or Fedora"
    exit 1
fi

# download and install epel repo
echo "Checking required repos..."
pushd /etc/yum.repos.d > /dev/null
if [ ! -f $epelRepo ] ; then
    echo "epel repo not found... Downloading and installing"
    if [ $osVersion -eq "6" ] ; then
        yum -y localinstall $epelRepoURL/$epelRPM6
    elif [ $osVersion -eq "7" ] ; then
        yum -y localinstall $epelRepoURL/e/$epelRPM7
    else
        echo "Unknown distro version. Manually install epel repo"
        exit 1
    fi
else
    echo "epel repo already installed"
fi
popd > /dev/null

# Update system 
yum -y update

# Download and install kimchi
echo "Installing kimchi..."
yum -y localinstall $kimchiURL/$kimchiRPM

# Setup firewall rules
echo "Opening firewall ports"
if [ $osVersion -eq "6" ] ; then
    iptables -A INPUT -p tcp --dport 8000 -j ACCEPT
    iptables -A INPUT -p tcp --dport 8001 -j ACCEPT 
    iptables -A INPUT -p tcp --dport 64667 -j ACCEPT
    service iptables save
    service iptables restart
elif [ $osVersion -eq "7" ] ; then
    firewall-cmd --add-port=8000/tcp --permanent
    firewall-cmd --add-port=8001/tcp --permanent
    firewall-cmd --add-port=64667/tcp --permanent
    firewall-cmd --reload
else
    echo "Unknown distro version. Open ports manually (tcp: 8000, 8001, 64667)"
fi

# Set proper SElinux permissions for kimchi?
echo "Applying required selinux policies"
pushd ../config/SElinux > /dev/null
semodule -i nginx.pp
popd > /dev/null

# Start kimchi
echo "Starting kimchi..."
if [ $osVersion -eq "6" ] ; then
    chkconfig on kimchid
    service kimchid start
elif [ $osVersion -eq "7" ] ; then
    systemctl enable kimchid
    systemctl start kimchid
else
    echo "Unknown distro version. Start manually kimchid"
fi

# Start libvirt-guests service
echo "Enabling and installing libvirt-guests. Setup in /etc/sysconfig/libvirt-guests"
if [ $osVersion -eq "6" ] ; then
    chkconfig on libvirt-guests
    service libvirt-guests start
elif [ $osVersion -eq "7" ] ; then
    systemctl enable libvirt-guests
    systemctl start libvirt-guests
else
    echo "Unknown distro version. Configure manually libvirt-guests service"
fi

echo "Installation finished. Access kimchi on http://$HOSTNAME:8000"

exit 0
