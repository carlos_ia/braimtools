#!/bin/bash
#
# Name:         bind_install
# Description:  Install ans setup bind using packages specifically build to use with samba
# OS Supported: RHEL and CentOS 6, 7 or 8
# Version:      1.00
# Autor:        Carlos Ibrahim Arias <carlos@braimtec.com>
# Based on:     -

### SET VARIABLES ###

srvIP="10.0.1.10"
subNet="$(echo "$srvIP" | awk -F. '{ print $1 "." $2 "." $3 }')"
cfgPath="../config/"
osDistro=$(cat /etc/*-release | grep ID_LIKE | awk -F\" '{ print $2 }' | awk '{ print $1 }')
osVersion=$(cat /etc/*-release | grep VERSION_ID | awk -F\" '{ print $2 }' | awk -F. '{ print $1 }')

### FUNCTIONS DEFINITION ###

# General function to install packages
function installPackage {
    if rpm -q --quiet $1; then 
        echo "Package $1 is already installed..."
    else 
        echo "$1 not found. Installing package..."
        yum install -y $1
    fi
}

# Check if user is root
function checkRoot {
    if [ $(id -u) -ne 0 ] ; then
       echo "User must be root to run this script"
       exit 1
    fi
}

# Check distro
function checkDistro {
    if [[ $osDistro != fedora && $osDistro != rhel ]] ; then
        echo "Linux distribution is not RHEL, CentOS or Fedora"
        exit 1
    fi
}

# Install Tranquil IT repo
function TranquilITrepoInstall {
    wget -O /etc/pki/rpm-gpg/RPM-GPG-KEY-TISSAMBA-8 https://samba.tranquil.it/RPM-GPG-KEY-TISSAMBA-8
    rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-TISSAMBA-8
    echo "[tis-samba]" > /etc/yum.repos.d/tissamba.repo
    echo "name=tis-samba" >> /etc/yum.repos.d/tissamba.repo
    echo "baseurl=https://samba.tranquil.it/redhat8/samba-4.16/" >> /etc/yum.repos.d/tissamba.repo
    echo "gpgcheck=1" >> /etc/yum.repos.d/tissamba.repo
    echo "gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-TISSAMBA-8" >> /etc/yum.repos.d/tissamba.repo
}

### START ###

checkRoot
checkDistro

# Show defined variables
echo "Server IP: $srvIP"
echo "Sub Net: $subNet"
read -p "Press [Enter] key to continue..."

# Install if package not installed
TranquilITrepoInstall
echo "Updating system"
yum update -y
installPackage bind
installPackage samba-dc-bind-dlz
installPackage bind-utils

# Setup firewall rules
echo "Opening firewall ports"
if [ $osVersion -eq "6" ] ; then
    iptables -A INPUT -p tcp --dport 53 -j ACCEPT
    iptables -A INPUT -p udp --dport 53 -j ACCEPT
    service iptables save
    service iptables restart
elif [ $osVersion -eq "7" ] || [ $osVersion -eq "8" ]; then
    firewall-cmd --add-port=53/tcp --permanent
    firewall-cmd --add-port=53/udp --permanent
    firewall-cmd --reload
else
    echo "Unknown distro version. Open ports manually (tcp, udp: 53)"
fi

# Setup named.conf
echo "Setting up named.conf..."
cp $cfgPath/named/named.conf /etc/named.conf
sed -i -e "s/<srvIP>/$srvIP/g" -e "s/<subNet>/$subNet/g" /etc/named.conf
chown root:named /etc/named.conf
chmod 640 /etc/named.conf
chcon -u system_u -r object_r -t named_conf_t /etc/named.conf

# Download list of root name servers
echo "Getting list of root name servers"
wget -q -O /var/named/named.root http://www.internic.net/zones/named.root

chown root:named /var/named/named.root
chmod 640 /var/named/named.root
chcon -u system_u -r object_r -t named_zone_t /var/named/named.root

# Create the localhost and 0.0.127.in-addr.arpa zone files
if [ ! -f /var/named/master ] ; then
    mkdir /var/named/master
fi

cp $cfgPath/named/master/* /var/named/master/

chown named:named /var/named/master -R
chmod 770 /var/named/master
chmod 640 /var/named/master/*.zone
chcon -u system_u -r object_r -t named_cache_t /var/named/master
chcon -u system_u -r object_r -t named_zone_t /var/named/master/* 

# Start the service and set it up to start on next boot
echo "Starting named.."
if [ $osVersion -eq "6" ] ; then
    chkconfig on named
    service named start
elif [ $osVersion -eq "7" ] || [ $osVersion -eq "8" ]; then
    systemctl enable named
    systemctl start named
else
    echo "Unknown distro version. Start manually named"
fi

echo "Creating symlink from /var/named to /srv/named"
ln -s /var/named /srv/named

# Test message
echo "Tests named using: 'host localhost. 127.0.0.1' or 'host 127.0.0.1 127.0.0.1'"
echo "and modify /etc/named.conf and /var/named/master/ to comply with your network settings"
echo "adding you domain forward and revers zones if needed. If planning to deploy a AD DC with samba"
echo "You must not add the AD domain forward or reverse zone records to the named.conf files,"
echo "these zones are stored dynamically in AD."
echo "Disable IPv6 Bind on the local network in /etc/sysconfig/named: OPTIONS=\"-4\""
echo "Check - https://samba.tranquil.it/doc/en/samba_config_server/redhat8/server_install_binddlz_redhat.html"

exit 0
